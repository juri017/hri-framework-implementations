#include "keyboard_task/User_Input.h"
#include "keyboard_task/Perception.h"

#include "Perception.h"

/* Perception class
 * Takes user input and packages relevant information for Event_Handler and Memory
 * Sub: User_Input
 * Pub: Event_Handler, Memory
 * Cli:
 * Srv:
 */

Perception::Perception()
{
    _pub = _n.advertise<keyboard_task::Perception>("perceived_data", 1);
    _sub = _n.subscribe<keyboard_task::User_Input>("input_data", 1, &Perception::callback, this);
}

void Perception::buildOutputMessage(const keyboard_task::User_Input msg)
{
    _perceptionMsg.Header.stamp = ros::Time::now();
    _perceptionMsg.Header.seq = msg.Header.seq + 1;
    _perceptionMsg.input = msg.input;
    _perceptionMsg.timeBetweenInputs = ros::Time::now() - _timeOfLastInput;
}

void Perception::callback(const keyboard_task::User_Input msg)
{    
    buildOutputMessage(msg);
    
    _pub.publish(_perceptionMsg);
    
    _timeOfLastInput = ros::Time::now();
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Perception");
    Perception Perception;
    
    ros::spin();
    return 0;
}
