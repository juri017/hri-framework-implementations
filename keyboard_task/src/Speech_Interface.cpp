#include "keyboard_task/Speech.h"

#include "Speech_Base.h"
#include "ASCII.h"

#include "Speech_Interface.h"

/* Speech Interface class
 * Executes preprogrammed robot speech
 * References Speech Base for available phrases
 * Sub: Action Center
 * Pub: 
 * Cli: 
 * Srv:
 */
 
Speech_Interface::Speech_Interface()
{
    _sub = _n.subscribe<keyboard_task::Speech>("speech", 1, &Speech_Interface::callback, this);
    
    puts(ASCII::SPEECH_OUTPUT_NODE);

    ros::Duration(15).sleep();

    system("clear");
    
    puts("Robot speech output:\n");
}

void Speech_Interface::callback(const keyboard_task::Speech msg)
{
    ROS_INFO_STREAM("SpeechID: " << msg.speechID);
    if(msg.speechID >= 0)
    {
        ROS_INFO_STREAM("if1");
        _speechPhrase = speechConstants::PHRASES[msg.speechID];
    }
    else
    {
        ROS_INFO_STREAM("if2");
        _speechPhrase = msg.speechPhrase;
    }
    system("clear");
    puts("Robot speech output:\n");
    puts(_speechPhrase.c_str());
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Speech_Interface");
    Speech_Interface Speech_Interface;
    
    ros::spin();
    return 0;
}
