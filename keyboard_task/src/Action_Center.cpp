#include "keyboard_task/Event.h"
#include "keyboard_task/Gesture.h"
#include "keyboard_task/Speech.h"
#include "keyboard_task/finalReactionTimeService.h"

#include "Social-Behavioral_Base.h"
#include "Task_Base.h"

#include <string>
#include <sstream>

#include "Action_Center.h"

/* Action Center class
 * Converts event requests into output action
 * Facilitates qualitative/quantitative feedback based on pre-determined parameters
 * Sub: Event Handler
 * Pub: Gesture Interface, Speech Interface
 * Cli: Memory
 * Srv: 
 */
 
Action_Center::Action_Center()
{
    _pubGesture = _n.advertise<keyboard_task::Gesture>("gesture", 1);
    _pubSpeech = _n.advertise<keyboard_task::Speech>("speech", 1);
    _sub = _n.subscribe<keyboard_task::Event>("event_data", 1, &Action_Center::callback, this);
    _finalReactionTimeCli = _n.serviceClient<keyboard_task::finalReactionTimeService>("finalReactionTime");
}

int Action_Center::requestFinalReactionTime(int taskNumber)
{
    _finalReactionTimeService.request.taskNumber = taskNumber;
    if(_finalReactionTimeCli.call(_finalReactionTimeService))
    {
        return (int)_finalReactionTimeService.response.finalReactionTime;
    }
    else
    {
        ROS_ERROR("Failed to call service finalReactionTimeService");
        return 0;
    }
}

void Action_Center::gestureFeedback(const keyboard_task::Event msg)
{
    switch(socialConstants::FEEDBACK_TYPE)
    {
    case 0:
        // Qualitative feedback
        if(msg.feedbackType == taskConstants::POSITIVE)
        {
            _gestureID = 3 * msg.feedbackType + msg.feedbackDegree;
        }
        else if(msg.feedbackType == taskConstants::NEGATIVE)
        {
            _gestureID = 3 * msg.feedbackType - msg.feedbackDegree;
        }
        else
        {
            _gestureID = 3 * msg.feedbackType;
        }
        break;
    case 1:
        // Quantitative feedback
        _gestureID = 3;
        break;
    }
}

void Action_Center::speechFeedback(const keyboard_task::Event msg)
{
    switch(socialConstants::FEEDBACK_TYPE)
    {
    case 0:
        // Qualitative feedback
        _speechID = _gestureID;
        break;
    case 1:
        // Quantitative feedback
        _speechID = -1;
        
        std::ostringstream oss;
        if(requestFinalReactionTime(1) == 0)
        {
            oss << "You had an average reaction time of " << requestFinalReactionTime(0) << " milliseconds. Try again to improve your score.";
        }
        else if(requestFinalReactionTime(0) < requestFinalReactionTime(1))
        {
            oss << "Your reaction time on this attempt was " << requestFinalReactionTime(1) - requestFinalReactionTime(0) << " milliseconds faster than your previous attempt.";
        }
        else if(requestFinalReactionTime(0) > requestFinalReactionTime(1))
        {
            oss << "Your reaction time on this attempt was " << requestFinalReactionTime(0) - requestFinalReactionTime(1) << " milliseconds slower than your previous attempt.";
        }
        else
        {
            oss << "Your reaction time on this attempt was the same as your previous attempt.";
        }
        _speechPhrase = oss.str();
        break;
    }
}

void Action_Center::buildOutputMessage(const keyboard_task::Event msg)
{
    _gestureMsg.Header.stamp = ros::Time::now();
    _gestureMsg.Header.seq = msg.Header.seq + 1;
    _gestureMsg.gestureID = _gestureID;
    
    _speechMsg.Header.stamp = ros::Time::now();
    _speechMsg.Header.seq = msg.Header.seq + 1;
    _speechMsg.speechID = _speechID;
    _speechMsg.speechPhrase = _speechPhrase;
}

void Action_Center::callback(const keyboard_task::Event msg)
{    
    gestureFeedback(msg);
    speechFeedback(msg);
    buildOutputMessage(msg);
    _pubGesture.publish(_gestureMsg);
    _pubSpeech.publish(_speechMsg);
    ROS_INFO_STREAM("I sent: " << _speechID);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Action_Center");
    Action_Center Action_Center;
    
    ros::spin();
    return 0;
}
