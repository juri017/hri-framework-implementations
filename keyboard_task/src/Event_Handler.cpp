#include "keyboard_task/Perception.h"
#include "keyboard_task/Event.h"
#include "keyboard_task/finalReactionTimeService.h"

#include "Task_Base.h"
#include "ASCII.h"

#include "Event_Handler.h"

/* Event Handler class
 * Handles the flow of the current task and decides when an event (action) needs to take place
 * Triggers robot output in the form of speech or gestures
 * Sub: Perception
 * Pub: Memory, Action Center
 * Cli: Memory
 * Srv: 
 */

Event_Handler::Event_Handler()
{
    _pub = _n.advertise<keyboard_task::Event>("event_data", 1);
    _sub = _n.subscribe<keyboard_task::Perception>("perceived_data", 1, &Event_Handler::callback, this);
    _finalReactionTimeCli = _n.serviceClient<keyboard_task::finalReactionTimeService>("finalReactionTime");
    
    _pubMemory = _n.advertise<std_msgs::Int32>("event_memory", 1);
    
    _difficulty = taskConstants::START_DIFFICULTY;
    _attemptCount = 0;
    resetTask();
    
    puts(ASCII::EVENT_HANDLER_NODE);

    ros::Duration(15).sleep();

    system("clear");
    printf("Starting Difficulty: %u\n", taskConstants::START_DIFFICULTY);
    puts("Press ENTER in the user interface window to begin!");
}

void Event_Handler::genCharList()
{
    srand(time(NULL));
    
    switch(_difficulty)
    {
    // Easy difficulty: only lowercase letters
    case 1:
        for(int i=0; i<taskConstants::STRING_LENGTH; ++i)
        {
            taskGlobals::charList[i] = 'a' + rand() % 26;
        }
        taskGlobals::charList[taskConstants::STRING_LENGTH] = '\0';
        break;
    
    // Medium difficulty: lowercase and uppercase letters
    case 2:
        for(int i=0; i<taskConstants::STRING_LENGTH; ++i)
        {
            taskGlobals::charList[i] = 'A' + rand() % 26 + rand() % 2 * 32;
        }
        taskGlobals::charList[taskConstants::STRING_LENGTH] = '\0';
        break;
    
    // Hard difficulty: all keyboard characters
    case 3:
        for(int i=0; i<taskConstants::STRING_LENGTH; ++i)
        {
            taskGlobals::charList[i] = '!' + rand() % 93;
        }
        taskGlobals::charList[taskConstants::STRING_LENGTH] = '\0';
        break;
    }
}

void Event_Handler::resetTask()
{
    _taskStep = 0;
    _firstInput = true;
        
    _wrongCount = 0;
    _slowCount = 0;
    _correctCount = 0;
    
    _totalInputTime = 0;
    _totalCorrectInputTime = 0;
    _finalInputTime = 0;
    
    genCharList();
    system("clear");
}

// Get expected reaction time for the task
int Event_Handler::getTaskReactionTime()
{
    return taskConstants::REACTION_TIME;
}

// Get expected key press from Task Base
char Event_Handler::getTaskChar()
{
    return taskGlobals::charList[_taskStep];
}

// Decide what kind of event should be triggered. Return integer between 0-2 for varying cases
int Event_Handler::decideEventTrigger(const keyboard_task::Perception msg)
{
    int output = 0;
    if(msg.input == getTaskChar())
    {
        output++;
        if(msg.timeBetweenInputs.toSec()*1000 - _promptDelay.toSec()*1000 <= getTaskReactionTime())
        {
            output++;
        }
    }
    return output;
}

void Event_Handler::updateCounters(const keyboard_task::Perception msg)
{
    _totalInputTime = msg.timeBetweenInputs.toSec()*1000 - _promptDelay.toSec()*1000 + _totalInputTime;
    switch(decideEventTrigger(msg))
    {
    // Bad key press
    case 0:
        _wrongCount++;
        break;
    // Slow key press
    case 1:
        _slowCount++;
        _totalCorrectInputTime = msg.timeBetweenInputs.toSec()*1000 - _promptDelay.toSec()*1000 + _totalCorrectInputTime;
        break;
    // Good key press
    case 2:
        _correctCount++;
        _totalCorrectInputTime = msg.timeBetweenInputs.toSec()*1000 - _promptDelay.toSec()*1000 + _totalCorrectInputTime;
        break;
    }
}

void Event_Handler::calculateFinalTime()
{
    if(_slowCount + _correctCount)
    {
        _finalInputTime = _totalCorrectInputTime / (_slowCount + _correctCount) + (_wrongCount * taskConstants::WRONG_PENALTY);
    }
    else
    {
        _finalInputTime = _wrongCount * taskConstants::WRONG_PENALTY;
    }
}

void Event_Handler::displayEndMessage()
{
    puts("");
    puts("+--------------------------+");
    puts("|  Congrats! You're done!  |");
    puts("+--------------------------+");
    puts("");
}

void Event_Handler::displayStats()
{
    system("clear");
    printf("Number of wrong characters: %u\n", _wrongCount);
    printf("Number of correct characters: %u\n", _correctCount + _slowCount);
    printf("Number of correct characters that were slow: %u \n", _slowCount);
    printf("Your final adjusted average time: %u\n", _finalInputTime);
}

int Event_Handler::requestFinalReactionTime(int taskNumber)
{
    _finalReactionTimeService.request.taskNumber = taskNumber;
    if(_finalReactionTimeCli.call(_finalReactionTimeService))
    {
        return (int)_finalReactionTimeService.response.finalReactionTime;
    }
    else
    {
        ROS_ERROR("Failed to call service finalReactionTimeService");
        return 0;
    }
}

int Event_Handler::calculateMultitaskFinalReactionTime(int multitaskNumber)
{
    int multitaskFinalReactionTime = 0;
    
    for(int i = 1; i <= multitaskNumber; i++)
    {
        if(requestFinalReactionTime(i - 1) == 0)
        {
            multitaskFinalReactionTime = 0;
            break;
        }
        multitaskFinalReactionTime += (requestFinalReactionTime(i - 1) - multitaskFinalReactionTime) / i;
    }
    return multitaskFinalReactionTime;
}

void Event_Handler::updateDifficulty()
{
    switch(taskConstants::DIFFICULTY_CHANGE)
    {
    case 1:
        if(_finalInputTime <= getTaskReactionTime())
        {
            if(_difficulty < 3)
            {
                _difficulty++;
            }
            break;
        }
        else if(_finalInputTime > 1.20*getTaskReactionTime())
        {
            if(_difficulty > 1)
            {
                _difficulty--;
            }
            break;
        }
        break;
    case 2:
        _tasksSinceDifficultyChange++;
        
        if (calculateMultitaskFinalReactionTime(taskConstants::MULTI_TASK_NUMBER) != 0 && _tasksSinceDifficultyChange >= taskConstants::MULTI_TASK_NUMBER)
        {
            if(_finalInputTime <= 0.90*getTaskReactionTime() && calculateMultitaskFinalReactionTime(taskConstants::MULTI_TASK_NUMBER) <= getTaskReactionTime())
            {
                if(_difficulty < 3)
                {
                    _difficulty++;
                    _tasksSinceDifficultyChange = 0;
                }
                break;
            }
            else if(_finalInputTime > 1.20*getTaskReactionTime() && calculateMultitaskFinalReactionTime(taskConstants::MULTI_TASK_NUMBER) > 1.10*getTaskReactionTime())
            {
                if(_difficulty > 1)
                {
                    _difficulty--;
                    _tasksSinceDifficultyChange = 0;
                }
                break;
            }
        }
        break;
    }
}

void Event_Handler::determineFeedbackType()
{
    if(_finalInputTime <= taskConstants::REACTION_TIME * 0.90)
    {
        _feedbackType = taskConstants::POSITIVE;
        _feedbackDegree = taskConstants::HIGH;
    }
    else if(_finalInputTime <= taskConstants::REACTION_TIME)
    {
        _feedbackType = taskConstants::POSITIVE;
        _feedbackDegree = taskConstants:: LOW;
    }
    else if(_finalInputTime <= taskConstants::REACTION_TIME * 1.20)
    {
        _feedbackType = taskConstants::NEUTRAL;
        _feedbackDegree = taskConstants::HIGH;
    }
    else
    {
        _feedbackType = taskConstants::NEGATIVE;
        _feedbackDegree = taskConstants::LOW;
    }
}

void Event_Handler::buildOutputMessage(const keyboard_task::Perception msg)
{
    _eventMsg.Header.stamp = ros::Time::now();
    _eventMsg.Header.seq = msg.Header.seq + 1;
    _eventMsg.feedbackType = _feedbackType;
    _eventMsg.feedbackDegree = _feedbackDegree;
    
    _memoryMsg.data = _finalInputTime;
}

bool Event_Handler::checkGiveFeedback()
{
    if(_attemptCount >= taskConstants::FEEDBACK_RATE)
    {
        _attemptCount = 0;
        return true;
    }
    else
    {
        return false;
    }
}

void Event_Handler::delayPrompt()
{
    if(taskConstants::RANDOM_PROMPT_DELAY)
    {
        _promptDelay = ros::Duration(rand() % (taskConstants::PROMPT_DELAY_RANDOM_HIGH - taskConstants::PROMPT_DELAY_RANDOM_LOW + 1) + taskConstants::PROMPT_DELAY_RANDOM_LOW);
    }
    else
    {
        _promptDelay = ros::Duration(taskConstants::PROMPT_DELAY_CONST);
    }
    _promptDelay.sleep();
}

void Event_Handler::callback(const keyboard_task::Perception msg)
{     
    if(!_firstInput)
    {
        updateCounters(msg);
        
        _taskStep++;
        if(getTaskChar() != '\0')
        {
            delayPrompt();
            printf("Type: %c\n", getTaskChar());
        }
        else
        {           
            calculateFinalTime();            
            displayEndMessage();
            
            ros::Duration(3).sleep();
            
            displayStats();
            
            ros::Duration(10).sleep();
            
            determineFeedbackType();
            buildOutputMessage(msg);
            _attemptCount++;
            
            if(checkGiveFeedback())
            {
                _pub.publish(_eventMsg);
            }
            _pubMemory.publish(_memoryMsg);
            
            updateDifficulty();
            resetTask();
            
            system("clear");
            printf("New Difficulty: %u \n", _difficulty);
            puts("Press ENTER in the user interface window to try again!");
        }
    }
    else if(_firstInput && msg.input == '\n')
    {
        delayPrompt();
        printf("Type: %c\n", getTaskChar());
        _firstInput = false;
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Event_Handler");
    Event_Handler Event_Handler;
    
    ros::spin();
    return 0;
}
