#include "keyboard_task/finalReactionTimeService.h"

#include "std_msgs/Duration.h"
#include "std_msgs/Int32.h"

#include "Memory.h"

/* Memory class
 * Stores data for use by other nodes and for analysis
 * Data can be extracted and analyzed using rosbag
 * Sub: Event Handler
 * Pub: 
 * Cli: 
 * Srv: Event Handler, Action Center
 */

Memory::Memory()
{
    _eventSub = _n.subscribe<std_msgs::Int32>("event_memory", 1, &Memory::eventCallback, this);
    _finalReactionTimeSrv = _n.advertiseService("finalReactionTime", &Memory::serviceFinalReactionTime, this);
}

bool Memory::serviceFinalReactionTime(keyboard_task::finalReactionTimeService::Request &req, keyboard_task::finalReactionTimeService::Response &res)
{
    if(req.taskNumber > _finalReactionTimes.size() - 1 || _finalReactionTimes.size() == 0)
    {
        res.finalReactionTime = 0;
    }
    else
    {
        res.finalReactionTime = _finalReactionTimes.at(_finalReactionTimes.size() - 1 - req.taskNumber);
    }
    ROS_INFO("request: Final Reaction Time for task attempt: %i", (int)req.taskNumber + 1);
    ROS_INFO("sending back response: [%i]", (int)res.finalReactionTime);
    return true;
}

void Memory::eventCallback(const std_msgs::Int32 msg)
{
    _finalReactionTimes.push_back(msg.data);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Memory");
    Memory Memory;
    
    ros::spin();
    return 0;
}
