#include "keyboard_task/User_Input.h"

#include "ASCII.h"

#include <signal.h>
#include <termios.h>
#include <stdio.h>

#include "User_Interface.h"

/* User_Interface class
 * Intercepts user keyboard inputs one at a time
 * Sub: 
 * Pub: Perception
 * Cli:
 * Srv:
 */

// Variables for interacting with the OS (not exactly sure how that works yet)
int kfd = 0;
struct termios cooked, raw;

User_Interface::User_Interface()
{
    _pub = _n.advertise<keyboard_task::User_Input>("input_data", 1);
    
    puts(ASCII::USER_INTERFACE_NODE);
    
    ros::Duration(15).sleep();
    
    system("clear");
}

void User_Interface::buildOutputMessage()
{
    _inputMsg.Header.stamp = ros::Time::now();
    _inputMsg.input = _inChar;
}

void User_Interface::getConsoleInRaw()
{
    tcgetattr(kfd, &cooked);
    memcpy(&raw, &cooked, sizeof(struct termios));
    raw.c_lflag &=~ (ICANON | ECHO);
}

void User_Interface::getKeyboardEvent()
{
    if(read(kfd, &_inChar, 1) < 0)
    {
        perror("read():");
        exit(-1);
    }
}

void User_Interface::keyLoop()
{
    getConsoleInRaw();
    
    // Setting a new line, then end of file
    raw.c_cc[VEOL] = 1;
    raw.c_cc[VEOF] = 2;
    tcsetattr(kfd, TCSANOW, &raw);

    puts("Reading from keyboard");
    puts("---------------------------");
    puts("Press a key to send a character. Press CTRL+C to quit.");


    for(;;)
    {
        getKeyboardEvent();
        buildOutputMessage();
        
        _pub.publish(_inputMsg);
        ROS_INFO_STREAM("Sent: " << _inChar);
    }
    return;
}

void quit(int sig)
{
    (void)sig;
    tcsetattr(kfd, TCSANOW, &cooked);
    ros::shutdown();
    exit(0);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "User_Interface");
    User_Interface User_Interface;
    
    signal(SIGINT, quit);
    
    User_Interface.keyLoop();
    
    return 0;
}
