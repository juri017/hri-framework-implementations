#ifndef MEMORY_H
#define MEMORY_H

#include "ros/ros.h"
#include <vector>

class Memory
{
public:
    Memory();
/*    ~Memory();*/
private:
    ros::NodeHandle _n;
    ros::Subscriber _perceptionSub;
    ros::Subscriber _eventSub;
    ros::ServiceServer _finalReactionTimeSrv;

/* Variables */
    
    // Array of recorded total reaction times for repeated task attempts
    std::vector<uint32_t> _finalReactionTimes;
    
/* Functions */
    
    /* bool serviceFinalReactionTime
     * Returns the final reaction time of the corresponding task number to the requestor
     */
    bool serviceFinalReactionTime(keyboard_task::finalReactionTimeService::Request &req, keyboard_task::finalReactionTimeService::Response &res);
    
    /* void eventCallback
     * Callback function that is triggered via subscription to Event
     */
    void eventCallback(const std_msgs::Int32 msg);
};

#endif
