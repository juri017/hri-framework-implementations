#ifndef ACTION_ENTER_H
#define ACTION_CENTER_H

#include "ros/ros.h"

class Action_Center
{
public:
    Action_Center();
/*    ~Action_Center();*/
private:
    ros::NodeHandle _n;
    ros::Subscriber _sub;
    ros::ServiceClient _finalReactionTimeCli;
    
    ros::Publisher _pubGesture;
    ros::Publisher _pubSpeech;
    
/* Variables */
    
    // Service class to be called when requesting data from Memory
    keyboard_task::finalReactionTimeService _finalReactionTimeService;

    
    // Output IDs
    int8_t _gestureID;
    int8_t _speechID;
    
    // Speech output phrase
    std::string _speechPhrase;
    
    // Messages to be published
    keyboard_task::Gesture _gestureMsg;
    keyboard_task::Speech _speechMsg;
    
/* Functions */
    
    /* int requestFinalReactionTime
     * Requests the final reaction time of the specified task number from Memory
     */
    int requestFinalReactionTime(int taskNumber);
    
    /* void gestureFeedback
     * Determines the appropriate gesture feedback output for the task
     * Reads from Social-Behavioral Base to determine intensity, qualitativeness, etc.
     */
    void gestureFeedback(const keyboard_task::Event msg);
    
    /* void speechFeedback
     * Determines the appropriate speech feedback output for the task
     * Reads from Social-Behavioral Base to determine intensity, qualitativeness, etc.
     */
    void speechFeedback(const keyboard_task::Event msg);
    
    /* void buildOutputMessage
     * Builds the output message to be published
     */
    void buildOutputMessage(const keyboard_task::Event msg);
    
    /* void callback
     * Callback function that is triggered when a message is received via subscription
     */
    void callback(const keyboard_task::Event msg);
};

#endif
