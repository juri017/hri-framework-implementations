#ifndef TASK_BASE_H
#define TASK_BASE_H

/* Constants */

namespace taskConstants
{    
    /* Task-Specific Constants */
    
    // Expected task reaction time
    const int REACTION_TIME = 1000;
    
    // Time penalty for getting a character wrong
    const int WRONG_PENALTY = 150;

    // Task character list
    const int STRING_LENGTH = 20;
    
    // Randomly timed prompts?
    const bool RANDOM_PROMPT_DELAY = true;
    
    // Random prompt delay bounds in seconds
    const int PROMPT_DELAY_RANDOM_LOW = 1;
    const int PROMPT_DELAY_RANDOM_HIGH = 3;
    
    // Prompt delay in seconds (if constant)
    const int PROMPT_DELAY_CONST = 1;


    /* General Task Constants */

    // Starting difficulty level of task
    const int START_DIFFICULTY = 1;
    
    // Difficulty change system (1: single-task control, 2: multi-task control)
    const int DIFFICULTY_CHANGE = 1;
    
    // Feedback rate (number of task attempts before feedback is given each time. 1 = every time, 2 = every other time, etc.)
    const int FEEDBACK_RATE = 1;
    
    // Number of tasks used for multi-task feedback
    const int MULTI_TASK_NUMBER = 3;

// DO NOT CHANGE ANYTHING BELOW THIS LINE
//------------------------------------------------------------------------------

    /* Constant definitions */
    
    // Feedback types
    const uint8_t POSITIVE = 0;
    const uint8_t NEUTRAL = 1;
    const uint8_t NEGATIVE = 2;
    
    // Feedback degrees
    const uint8_t HIGH = 0;
    const uint8_t LOW = 1;
}

/* Global variables */

namespace taskGlobals
{
    /* Task-Specific Globals */
    
    char charList[taskConstants::STRING_LENGTH];
}

#endif
