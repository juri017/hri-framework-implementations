#ifndef PERCEPTION_H
#define PERCEPTION_H

#include "ros/ros.h"
#include <std_msgs/Duration.h>

class Perception
{
public:
    Perception();
/*    ~Perception();*/
private:
    ros::NodeHandle _n;
    ros::Publisher _pub;
    ros::Subscriber _sub;
    
/* Variables */

    // Time of the last user inout
    ros::Time _timeOfLastInput;
    
    // Message to be published
    keyboard_task::Perception _perceptionMsg;
    
    // Message to be published to Action Center
    std_msgs::Duration _memoryMsg;

/* Functions */

    /* void buildOutputMessage
     * Builds the output message to be published
     */
    void buildOutputMessage(const keyboard_task::User_Input msg);
    
    /* void callback
     * Callback function that is triggered when a message is received via subscription
     */
    void callback(const keyboard_task::User_Input msg);
};

#endif
