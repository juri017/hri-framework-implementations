#ifndef GESTURE_INTERFACE_H
#define GESTURE_INTERFACE_H

#include <string>
#include "ros/ros.h"

class Gesture_Interface
{
public:
    Gesture_Interface();
/*    ~Gesture_Interface();*/
private:
    ros::NodeHandle _n;
    ros::Subscriber _sub;
    
/* Variables */
    
    // Description of gesture that would be executed on the actual robot
    std::string _gestureDescription;
    
/* Functions */
    
    /* void callback
     * Callback function that is triggered when a message is received via subscription
     */
    void callback(const keyboard_task::Gesture msg);
};

#endif
