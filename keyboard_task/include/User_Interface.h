#ifndef USER_INTERFACE_H
#define USER_INTERFACE_H

#include "ros/ros.h"

class User_Interface
{
public:
    User_Interface();
/*    ~User_Interface*/
    
    /* void keyLoop
     * Accepts user keyboard input one character at a time
     * Interacts with the OS to intercept keyboard inputs
     */
    void keyLoop();
private:
    ros::NodeHandle _n;
    ros::Publisher _pub;

/* Variables */

    // Most recent user input
    char _inChar;
    
    // Message to be published
    keyboard_task::User_Input _inputMsg;
    
/* Functions */

    /* void buildOutputMessage
     * Builds the output message to be published
     */
    void buildOutputMessage();

    /* void getConsoleInRaw
     * Gets the console in raw mode
     */
    void getConsoleInRaw();
    
    /* void getKeyboardEvent
     * Gets the next event from the keyboard
     */
    void getKeyboardEvent();
};

#endif
