#ifndef GESTURE_BASE_H
#define GESTURE_BASE_H

#include <string>

/* Constants */

namespace gestureConstants
{
    std::string GESTURE_1 = "The Robot waves its arms above its head and dances excitedly";
    std::string GESTURE_2 = "The Robot punches the air with confidence";
    std::string GESTURE_3 = "The Robot claps its hands in encouragement";
    std::string GESTURE_4 = "The Robot nods its head in satisfaction";
    std::string GESTURE_5 = "The Robot shrugs its shoulders in indifference";
    std::string GESTURE_6 = "The Robot hangs its head in disappointment";
    std::string GESTURE_7 = "The Robot sits down and begins to cry";
    
    std::string GESTURES[7] = {GESTURE_1, GESTURE_2, GESTURE_3, GESTURE_4, GESTURE_5, GESTURE_6, GESTURE_7};
}

/* Global variables */

namespace gestureGlobals
{
    
}

#endif
