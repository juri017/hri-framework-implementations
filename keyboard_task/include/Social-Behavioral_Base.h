#ifndef SOCIAL_BEHAVIORAL_BASE_H
#define SOCIAL_BEHAVIORAL_BASE_H

/* Constants */

namespace socialConstants
{
    /* General Task Constants */
    
    // Feedback type (0 = qualitative, 1 = quantitative)
    const int FEEDBACK_TYPE = 1;
}

/* Global variables */

namespace socialGlobals
{
    
}

#endif
