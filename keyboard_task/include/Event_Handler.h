#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include "ros/ros.h"
#include <std_msgs/Int32.h>

#include "Task_Base.h"

class Event_Handler
{
public:
    Event_Handler();
/*    ~Event_Handler();*/
private:
    ros::NodeHandle _n;
    ros::Publisher _pub;
    ros::Subscriber _sub;
    ros::ServiceClient _finalReactionTimeCli;
    
    ros::Publisher _pubMemory;
    
/* Variables */
    
    // The task step number that the user is on
    int _taskStep;
    
    // Flag for first key press to start
    bool _firstInput;
    
    // Time delay before next input prompt
    ros::Duration _promptDelay;
    
    // Counters for three input types
    int _wrongCount;
    int _slowCount;
    int _correctCount;
    
    // Total user input time over the course of the task (summation of each task step)
    int _totalInputTime;
    
    // Total user input time for correct inputs only
    int _totalCorrectInputTime;
    
    // Final input time used to determine new difficulty (includes penalties)
    int _finalInputTime;
    
    // Current difficulty of task
    int _difficulty;
    
    // Counter that tracks the number of tasks attempted at the current difficulty level
    int _tasksSinceDifficultyChange;
    
    // Feedback parameters
    uint8_t _feedbackType;
    uint8_t _feedbackDegree;
    
    // Number of attempts since last feedback
    int _attemptCount;
    
    // Message to be published to Action Center
    keyboard_task::Event _eventMsg;
    
    // Service class to be called when requesting data from Memory
    keyboard_task::finalReactionTimeService _finalReactionTimeService;
    
    // Message to be published to Memory
    std_msgs::Int32 _memoryMsg;
    
/* Functions */

    /* void genCharList
     * Generates a list of random keyboard characters that varies with the difficulty
     */
    void genCharList();
    
    /* void resetTask
     * Resets the task, clearing all flags and counters
     * Generates a new character list
     */
    void resetTask();
    
    /* int getTaskReactionTime
     * Returns the expected average reaction time based on the user's age
     */
    int getTaskReactionTime();
    
    /* char getTaskChar
     * Returns the character of the random sequence at the given task step
     */
    char getTaskChar();
    
    /* int decideEventTrigger
     * Decides what kind of event should be triggered. Returns integer between 0-2 for varying cases
     */
    int decideEventTrigger(const keyboard_task::Perception msg);
    
    /* void updateCounters
     * Updates counters and time totals
     */
    void updateCounters(const keyboard_task::Perception msg);
    
    /* void calculateFinalTime
     * Calculates the user's final time including penalties
     */
    void calculateFinalTime();
    
    /* void displayEndMessage
     * Displays a message indicating that the user has completed the task
     */
    void displayEndMessage();
    
    /* void displayStats
     * Displays the user's performance statistics from the task
     */
    void displayStats();
    
    /* int requestFinalReactionTime
     * Requests the final reaction time of the specified task number from Memory
     */
    int requestFinalReactionTime(int taskNumber);
    
    /* int calculateMultitaskFinalReactionTime
     * Calculates the average final reaction time over the past ____ tasks
     * Number of tasks is specified by MULTI_TASK_NUMBER in Task_Base.h
     */
    int calculateMultitaskFinalReactionTime(int multitaskNumber);
    
    /* void updateDifficulty
     * Updates the difficulty based on logic
     */
    void updateDifficulty();
    
    /* int determineFeedbackType
     * Determines the type of feedback to be given to the user
     * All logic for positive/negative feedback control would go here
     */
    void determineFeedbackType();
    
    /* void buildOutputMessage
     * Builds the output message to be published
     */
    void buildOutputMessage(const keyboard_task::Perception msg);
    
    /* bool checkGiveFeedback
     * Checks if feedback should be given after the last task attempt
     */
    bool checkGiveFeedback();
    
    /* void delayPrompt()
     * Sleeps for amount of time set by _promptDelay
     */
    void delayPrompt();
    
    /* void callback
     * Callback function that is triggered when a message is received via subscription
     */
    void callback(const keyboard_task::Perception msg);
};

#endif
