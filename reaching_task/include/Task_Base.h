#ifndef TASK_BASE_H
#define TASK_BASE_H

/* Constants */

namespace taskConstants
{    
    /* Task-Specific Constants */
    
    // Expected task reach time
    const float REACH_TIME = 1;
    
    // Number of trial reaches for calibration
    const int CALIBRATION_NUMBER = 5;
    
    // Threshold of z-axis angular velocity movement to be considered motion (in rad/s)
    const float MOTION_THRESHOLD = 0.1;
    
    // Number of data points where motion is beneath the threshold to consider the reach ended (no motion)
    const int NO_MOTION_DATA_NUMBER = 200;


    /* General Task Constants */

    // Starting difficulty level of task
    const int START_DIFFICULTY = 1;
    
    // Difficulty change system (1: single-task control, 2: multi-task control)
    const int DIFFICULTY_CHANGE = 1;
    
    // Feedback rate (number of task attempts before feedback is given each time. 1 = every time, 2 = every other time, etc.)
    const int FEEDBACK_RATE = 3;
    
    // Number of tasks used for multi-task feedback
    const int MULTI_TASK_NUMBER = 3;

// DO NOT CHANGE ANYTHING BELOW THIS LINE
//------------------------------------------------------------------------------

    /* Constant definitions */
    
    // Feedback types
    const uint8_t POSITIVE = 0;
    const uint8_t NEUTRAL = 1;
    const uint8_t NEGATIVE = 2;
    
    // Feedback degrees
    const uint8_t HIGH = 0;
    const uint8_t LOW = 1;
}

/* Global variables */

namespace taskGlobals
{
    /* Task-Specific Globals */
    double CALIBRATED_REACH_TIME;
}

#endif
