#ifndef MEMORY_H
#define MEMORY_H

#include "ros/ros.h"
#include <vector>

class Memory
{
public:
    Memory();
/*    ~Memory();*/
private:
    ros::NodeHandle _n;
    ros::Subscriber _sub;
    ros::ServiceServer _reachTimeSrv;
    ros::ServiceServer _smaSrv;

/* Variables */
    
    // Array of recorded reach times for repeated task attempts
    std::vector<double> _reachTimes;
    
    // Array of recorded signal magnitude areas for repeated task attempts
    std::vector<uint32_t> _smas;
    
/* Functions */
    
    /* bool serviceReachTime
     * Returns the reach time of the corresponding task number to the requestor
     */
    bool serviceReachTime(reaching_task::reachTimeService::Request &req, reaching_task::reachTimeService::Response &res);
    
    /* bool serviceSMA
     * Returns the sma of the corresponding task number to the requestor
     */
    bool serviceSMA(reaching_task::smaService::Request &req, reaching_task::smaService::Response &res);
    
    /* void callback
     * Callback function that is triggered via subscription to Event Handler
     */
    void callback(const reaching_task::Reach_Data msg);
};

#endif
