#ifndef PERCEPTION_H
#define PERCEPTION_H

#include "ros/ros.h"
#include <std_msgs/Duration.h>

class Perception
{
public:
    Perception();
/*    ~Perception();*/
private:
    ros::NodeHandle _n;
    ros::Publisher _pubInput;
    ros::Publisher _pubData;
    ros::Subscriber _subInput;
    ros::Subscriber _subData;
    
/* Variables */
    
    // Message to be published with user input data
    reaching_task::Input _msgInput;
    
    // Message to be published with motion data
    reaching_task::Data _msgData;

/* Functions */

    /* void buildOutputMessageInput
     * Builds the output message to be published for user input data
     */
    void buildOutputMessageInput(const reaching_task::User_Input msg);
    
    /* void buildOutputMessageData
     * Builds the output message to be published for velocity data
     */
    void buildOutputMessageData(const sensor_msgs::Imu msg);
    
    /* void callbackInput
     * Callback function that is triggered when a message is received with user input data
     */
    void callbackInput(const reaching_task::User_Input msg);
    
    /* void callbackData
     * Callback function that is triggered when a message is received with IMU data
     */
    void callbackData(const sensor_msgs::Imu msg);
};

#endif
