#ifndef SPEECH_BASE_H
#define SPEECH_BASE_H

#include <string>

/* Constants */

namespace speechConstants
{
    std::string SPEECH_1 = "\"Fantastic Job! You nailed it!\"";
    std::string SPEECH_2 = "\"Nice Work! Let's keep going!\"";
    std::string SPEECH_3 = "\"Well done. Keep it up.\"";
    std::string SPEECH_4 = "\"Good work. Let's try one more.\"";
    std::string SPEECH_5 = "\"Not bad. Let's try again.\"";
    std::string SPEECH_6 = "\"Come on. You can do better than that.\"";
    std::string SPEECH_7 = "\"I was hoping you'd have done better than that this time.\"";
    
    std::string PHRASES[7] = {SPEECH_1, SPEECH_2, SPEECH_3, SPEECH_4, SPEECH_5, SPEECH_6, SPEECH_7};
}

/* Global variables */

namespace speechGlobals
{
    
}

#endif
