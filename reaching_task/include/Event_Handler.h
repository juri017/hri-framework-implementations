#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include "ros/ros.h"
#include <vector>

#include "Task_Base.h"

class Event_Handler
{
public:
    Event_Handler();
/*    ~Event_Handler();*/
private:
    ros::NodeHandle _n;
    ros::Publisher _pubEvent;
    ros::Publisher _pubData;
    ros::Subscriber _subInput;
    ros::Subscriber _subData;
    ros::ServiceClient _reachTimeCli;
    ros::ServiceClient _smaCli;
    
    ros::Publisher _pubMemory;
    
/* Variables */
    
    // Flag for first key press to start
    bool _firstInput;
    
    // Time delay before next input prompt
    ros::Duration _promptDelay;
    
    // Final input time used to determine new difficulty (includes penalties)
    int _finalInputTime;
    
    // Current difficulty of task
    int _difficulty;
    
    // Counter that tracks the number of tasks attempted at the current difficulty level
    int _tasksSinceDifficultyChange;
    
    // Feedback parameters
    uint8_t _feedbackType;
    uint8_t _feedbackDegree;
    
    // Number of attempts since last feedback
    int _attemptCount;

    // Boolean that is true while a reach is in progresss
    bool _reaching;
    
    // Boolean that is true once the user's reach has been calibrated
    bool _calibrated;
    
    // Counter that keeps track of the number of reaches for calibration
    int _calibrationCount;
    
    // true if the reach attempt was bad (not enough movement to count as a reach or couldn't calculate reach time
    bool _badReach;
    
    // Total time of the reach
    ros::Duration _reachTime;
    
    // Signal magnitude area of motion data
    float _sma;
    
    // Data arrays
    std::vector<float> _xData;
    std::vector<float> _yData;
    std::vector<float> _zData;
    
    // Time array
    std::vector<ros::Time> _times;
    
    // Message to be published to Action Center
    reaching_task::Event _msgEvent;
    
    // Message to be published to Memory to store data
    reaching_task::Reach_Data _msgData;
    
    // Service class to be called when requesting reach time from Memory
    reaching_task::reachTimeService _reachTimeService;
    
    // Service class to be called when requesting sma from Memory
    reaching_task::smaService _smaService;
    
/* Functions */
    
    /* void resetTask
     * Resets the task, clearing all flags and counters
     * Generates a new character list
     */
    void resetTask();
    
    /* void displayEndMessage
     * Displays a message indicating that the user has completed the task
     */
    void displayEndMessage();
    
    /* double requestReachTime
     * Requests the reach time of the specified task number from Memory
     */
    double requestReachTime(int taskNumber);
    
    /* int requestSMA
     * Requests the sma of the specified task number from Memory
     */
    int requestSMA(int taskNumber);
    
    /* double calculateMultitaskReachTime
     * Calculates the average reach time over the past ____ task attempts
     * Number of tasks is specified by MULTI_TASK_NUMBER in Task_Base.h
     */
    double calculateMultitaskReachTime(int multitaskNumber);
    
    /* void updateDifficulty
     * Updates the difficulty based on logic
     */
    void updateDifficulty();
    
    /* int determineFeedbackType
     * Determines the type of feedback to be given to the user
     * All logic for positive/negative feedback control would go here
     */
    void determineFeedbackType();
    
    /* bool checkGiveFeedback
     * Checks if feedback should be given after the last task attempt
     */
    bool checkGiveFeedback();
    
    /* void delayPrompt()
     * Sleeps for amount of time set by _promptDelay
     */
    void delayPrompt();
    
    /* void displayInstructions()
     * Displays the task instructions to the user
     */
    void displayInstructions();
    
    /* void calculateSMA()
     * Calculates the signal magnitude area of motion data
     */
    void calculateSMA();
    
    /* void calculateReachTime()
     * Calculates the reach time of motion data
     */
    void calculateReachTime();
    
    /* void buildOutputMessageEvent
     * Builds the output message to be published for an event
     */
    void buildOutputMessageEvent(const reaching_task::Input msg);
    
    /* void buildOutputMessageData
     * Builds the output message to be published for reach data
     */
    void buildOutputMessageData();
    
    /* void callbackInput
     * Callback function that is triggered when a message is received via subscription from input
     */
    void callbackInput(const reaching_task::Input msg);
    
    /* void callbackData
     * Callback function that is triggered when a message is received via subscription from data
     */
    void callbackData(const reaching_task::Data msg);
};

#endif
