#ifndef SPEECH_INTERFACE_H
#define SPEECH_INTERFACE_H

#include <string>
#include "ros/ros.h"

class Speech_Interface
{
public:
    Speech_Interface();
/*    ~Speech_Interface();*/
private:
    ros::NodeHandle _n;
    ros::Subscriber _sub;
    
/* Variables */
    
    // Phrase that would be spoken by the actual robot
    std::string _speechPhrase;
    
/* Functions */
    
    /* void callback
     * Callback function that is triggered when a message is received via subscription
     */
    void callback(const reaching_task::Speech msg);
};

#endif
