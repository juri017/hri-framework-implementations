#include "reaching_task/Input.h"
#include "reaching_task/Data.h"
#include "reaching_task/Event.h"
#include "reaching_task/reachTimeService.h"
#include "reaching_task/smaService.h"
#include "reaching_task/Reach_Data.h"

#include "Task_Base.h"
#include "ASCII.h"

#include "Event_Handler.h"

/* Event Handler class
 * Handles the flow of the current task and decides when an event (action) needs to take place
 * Triggers robot output in the form of speech or gestures
 * Sub: Perception
 * Pub: Memory, Action Center
 * Cli: Memory
 * Srv: 
 */

Event_Handler::Event_Handler()
{
    _pubEvent = _n.advertise<reaching_task::Event>("event_data", 1);
    _pubData = _n.advertise<reaching_task::Reach_Data>("reach_data", 1);
    _subInput = _n.subscribe<reaching_task::Input>("input_data", 1, &Event_Handler::callbackInput, this);
    _subData = _n.subscribe<reaching_task::Data>("velocity_data", 1, &Event_Handler::callbackData, this);
    _reachTimeCli = _n.serviceClient<reaching_task::reachTimeService>("reachTime");
    _smaCli = _n.serviceClient<reaching_task::smaService>("sma");
    
    _difficulty = taskConstants::START_DIFFICULTY;
    _attemptCount = 0;
    _calibrated = false;
    resetTask();
    
    puts(ASCII::EVENT_HANDLER_NODE);

    ros::Duration(10).sleep();

    system("clear");
    
    ros::Duration(3).sleep();
    
    puts("We will begin by calibrating your reaching motion.");
    ros::Duration(2).sleep();
    puts("");
    puts("");
    displayInstructions();
    puts("");
    puts("");
    puts("Press ENTER in the user interface window to begin.");
}

void Event_Handler::resetTask()
{
    _firstInput = true;
    _reaching = false;
    _badReach = false;
    
    _reachTime = ros::Duration(0);
    _sma = 0;
    
    _xData.clear();
    _yData.clear();
    _zData.clear();
    
    _times.clear();
    
    system("clear");
}

void Event_Handler::displayEndMessage()
{
    puts("");
    puts("+--------------------------+");
    puts("|  Congrats! You're done!  |");
    puts("+--------------------------+");
    puts("");
}

double Event_Handler::requestReachTime(int taskNumber)
{
    _reachTimeService.request.taskNumber = taskNumber;
    if(_reachTimeCli.call(_reachTimeService))
    {
        return _reachTimeService.response.reachTime;
    }
    else
    {
        ROS_ERROR("Failed to call service reachTimeService");
        return 0;
    }
}

int Event_Handler::requestSMA(int taskNumber)
{
    _smaService.request.taskNumber = taskNumber;
    if(_smaCli.call(_smaService))
    {
        return (int)_smaService.response.sma;
    }
    else
    {
        ROS_ERROR("Failed to call service smaService");
        return 0;
    }
}

double Event_Handler::calculateMultitaskReachTime(int multitaskNumber)
{
    int multitaskReachTime = 0;
    
    for(int i = 1; i <= multitaskNumber; i++)
    {
        if(requestReachTime(i - 1) == 0)
        {
            multitaskReachTime = 0;
            break;
        }
        multitaskReachTime += (requestReachTime(i - 1) - multitaskReachTime) / i;
    }
    return multitaskReachTime;
}

//void Event_Handler::updateDifficulty()
//{
//    switch(taskConstants::DIFFICULTY_CHANGE)
//    {
//    case 1:
//        if(_finalInputTime <= getTaskReactionTime())
//        {
//            if(_difficulty < 3)
//            {
//                _difficulty++;
//            }
//            break;
//        }
//        else if(_finalInputTime > 1.20*getTaskReactionTime())
//        {
//            if(_difficulty > 1)
//            {
//                _difficulty--;
//            }
//            break;
//        }
//        break;
//    case 2:
//        _tasksSinceDifficultyChange++;
//        
//        if (calculateMultitaskFinalReactionTime(taskConstants::MULTI_TASK_NUMBER) != 0 && _tasksSinceDifficultyChange >= taskConstants::MULTI_TASK_NUMBER)
//        {
//            if(_finalInputTime <= 0.90*getTaskReactionTime() && calculateMultitaskFinalReactionTime(taskConstants::MULTI_TASK_NUMBER) <= getTaskReactionTime())
//            {
//                if(_difficulty < 3)
//                {
//                    _difficulty++;
//                    _tasksSinceDifficultyChange = 0;
//                }
//                break;
//            }
//            else if(_finalInputTime > 1.20*getTaskReactionTime() && calculateMultitaskFinalReactionTime(taskConstants::MULTI_TASK_NUMBER) > 1.10*getTaskReactionTime())
//            {
//                if(_difficulty > 1)
//                {
//                    _difficulty--;
//                    _tasksSinceDifficultyChange = 0;
//                }
//                break;
//            }
//        }
//        break;
//    }
//}

void Event_Handler::determineFeedbackType()
{
    if(_reachTime.toSec() <= taskConstants::REACH_TIME * 0.90)
    {
        _feedbackType = taskConstants::POSITIVE;
        _feedbackDegree = taskConstants::HIGH;
    }
    else if(_reachTime.toSec()  <= taskConstants::REACH_TIME)
    {
        _feedbackType = taskConstants::POSITIVE;
        _feedbackDegree = taskConstants:: LOW;
    }
    else if(_reachTime.toSec()  <= taskConstants::REACH_TIME * 1.20)
    {
        _feedbackType = taskConstants::NEUTRAL;
        _feedbackDegree = taskConstants::HIGH;
    }
    else
    {
        _feedbackType = taskConstants::NEGATIVE;
        _feedbackDegree = taskConstants::LOW;
    }
}

bool Event_Handler::checkGiveFeedback()
{
    if(_attemptCount >= taskConstants::FEEDBACK_RATE)
    {
        _attemptCount = 0;
        return true;
    }
    else
    {
        return false;
    }
}

//void Event_Handler::delayPrompt()
//{
//    if(taskConstants::RANDOM_PROMPT_DELAY)
//    {
//        _promptDelay = ros::Duration(rand() % (taskConstants::PROMPT_DELAY_RANDOM_HIGH - taskConstants::PROMPT_DELAY_RANDOM_LOW + 1) + taskConstants::PROMPT_DELAY_RANDOM_LOW);
//    }
//    else
//    {
//        _promptDelay = ros::Duration(taskConstants::PROMPT_DELAY_CONST);
//    }
//    _promptDelay.sleep();
//}

void Event_Handler::displayInstructions()
{
    puts("INSTRUCTIONS");
    puts("");
    puts("Start with the index finger of your right hand on the marker directly in front of you ('MARKER 0') and your left hand on the spacebar of the laptop keyboard.");
    puts("When prompted to perform a reach, press the spacebar with your left hand and then, when you are ready, reach your right hand to touch the specified marker with your index finger as quickly as possible.");
    puts("Return your index finger back to 'MARKER 0' as quickly as possible, then press the spacebar again with your left hand to finish the reach.");
    printf("To calibrate your motion you will be reaching to 'MARKER 1' %i times in total. You will be prompted before each reach.\n", taskConstants::CALIBRATION_NUMBER);
}

void Event_Handler::calculateSMA()
{
    float sum = 0;
    for(int i = 0; i < _xData.size(); i++)
    {
        sum += abs(_xData.at(i) - _xData.at(0) ) + abs(_yData.at(i) - _yData.at(0)) + abs(_zData.at(i) - _zData.at(0));
    }
    _sma = sum / _xData.size();
}

void Event_Handler::calculateReachTime()
{
    ros::Time startTime;
    ros::Time endTime;
    bool startFound = false;
    bool endFound = false;
    int noMotionCount = 0;
    
    for(int i = 0; i < taskConstants::NO_MOTION_DATA_NUMBER; i++)
    {
        _zData.push_back(0);
    }
    
    for(int i = 0; i < _zData.size(); i++)
    {
        if(!startFound && abs(_zData.at(i)) >= taskConstants::MOTION_THRESHOLD)
        {
            startTime = _times.at(i);
            startFound = true;
        }
        else if(startFound && noMotionCount < taskConstants::NO_MOTION_DATA_NUMBER)
        {
            if(abs(_zData.at(i)) <= taskConstants::MOTION_THRESHOLD)
            {
                noMotionCount++;
            }
            else
            {
                noMotionCount = 0;
            }
        }
        else if(startFound && noMotionCount >= taskConstants::NO_MOTION_DATA_NUMBER)
        {
            endTime = _times.at(i - taskConstants::NO_MOTION_DATA_NUMBER);
            endFound = true;
            break;
        }
    }
    
    if(!startFound || !endFound)
    {
        _badReach = true;
    }
    
    _reachTime = endTime - startTime;
//    ROS_INFO_STREAM("Start time: " << startTime);
//    ROS_INFO_STREAM("End time: " << endTime);
}

void Event_Handler::buildOutputMessageData()
{
    _msgData.Header.stamp = ros::Time::now();
    _msgData.sma = _sma;
    _msgData.reachTime = _reachTime.toSec();
}

void Event_Handler::buildOutputMessageEvent(const reaching_task::Input msg)
{
    _msgEvent.Header.stamp = ros::Time::now();
    _msgEvent.Header.seq = msg.Header.seq + 1;
    _msgEvent.feedbackType = _feedbackType;
    _msgEvent.feedbackDegree = _feedbackDegree;
}

void Event_Handler::callbackInput(const reaching_task::Input msg)
{     
    // Initial input to begin the task
    if(_firstInput && msg.input == '\n')
    {
        _firstInput = false;
        system("clear");
        puts("Press SPACE to begin the reach.");
    }
    // First spacebar press, which begins the reach
    else if(!_firstInput && !_reaching && msg.input == 32)
    {
        _reaching = true;
        puts("Press SPACE again to end the reach.");
        puts("");
    }
    // Second spacebar press, which ends the reach
    else if(!_firstInput && _reaching && msg.input == 32)
    {
        _reaching = false;
        
//      calculateSMA();
        calculateReachTime();
        
        if(!_badReach)
        {
            ROS_INFO_STREAM("Reach Time: " << _reachTime.toSec());
                
            buildOutputMessageData();
            _pubData.publish(_msgData);
            ROS_INFO_STREAM("Published reach time:" << _reachTime.toSec());
            
            ros::Duration(2).sleep();
            
            if(_calibrated)
            {
//                displayEndMessage();
                            
                determineFeedbackType();
                buildOutputMessageEvent(msg);
                _attemptCount++;
                
                if(checkGiveFeedback())
                {
                    _pubEvent.publish(_msgEvent);
                }
                
    //            updateDifficulty();
                resetTask();
                
                system("clear");
    //            printf("New Difficulty: %u \n", _difficulty);
                puts("Press ENTER in the user interface window to try again.");
            }
            // Calibration in progress
            else
            {
                _calibrationCount++;
                
                printf("Calibration reaches completed: %i/%i\n", _calibrationCount, taskConstants::CALIBRATION_NUMBER);
                
                if(_calibrationCount >= taskConstants::CALIBRATION_NUMBER)
                {
                    _calibrated = true;
                    
                    ros::Duration(3).sleep();
                    
                    puts("");
                    puts("Calibration complete. Begin recorded reach attempts");
                    
                    taskGlobals::CALIBRATED_REACH_TIME = calculateMultitaskReachTime(taskConstants::CALIBRATION_NUMBER);
                    
                    ROS_INFO_STREAM("Calibrated reach time: " << taskGlobals::CALIBRATED_REACH_TIME);
                }
                
                ros::Duration(4).sleep();
                
                resetTask();
                system("clear");
    //            printf("New Difficulty: %u \n", _difficulty);
                puts("Press ENTER in the user interface window to try again.");
            }
        }
        else
        {
            puts("Bad reach. Try the reach again.");
        }
    }
}

void Event_Handler::callbackData(const reaching_task::Data msg)
{
    if(_reaching)
    {
//        system("cd ~/bagfiles");
//        system("rosbag record -q -o Motion_Data /imu/data_raw __name:=bag_recording");
//        
//        ROS_INFO_STREAM("Recording Started");
        
        _xData.push_back(msg.data.x);
        _yData.push_back(msg.data.y);
        _zData.push_back(msg.data.z);
        
        _times.push_back(ros::Time::now());
    }
//    if(!_reaching && _stopRecording)
//    {
//        system("rosnode kill /reaching_task/bag_recording");
//        
//        ROS_INFO_STREAM("Recording Stopped");

//        _stopRecording = false;
//    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Event_Handler");
    Event_Handler Event_Handler;
    
    ros::spin();
    return 0;
}
