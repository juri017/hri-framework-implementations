#include "reaching_task/Gesture.h"

#include "Gesture_Base.h"
#include "ASCII.h"

#include "Gesture_Interface.h"

/* Gesture Interface class
 * Executes preprogrammed robot gestures
 * References Gesture Base for available gestures
 * Sub: Action Center
 * Pub: 
 * Cli: 
 * Srv:
 */
 
Gesture_Interface::Gesture_Interface()
{
    _sub = _n.subscribe<reaching_task::Gesture>("gesture", 1, &Gesture_Interface::callback, this);

    puts(ASCII::GESTURE_OUTPUT_NODE);

    ros::Duration(15).sleep();

    system("clear");
    
    puts("Robot gesture output:\n");
}

void Gesture_Interface::callback(const reaching_task::Gesture msg)
{
    _gestureDescription = gestureConstants::GESTURES[msg.gestureID];
    system("clear");
    puts("Robot gesture output:\n");
    puts(_gestureDescription.c_str());
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Gesture_Interface");
    Gesture_Interface Gesture_Interface;
    
    ros::spin();
    return 0;
}
