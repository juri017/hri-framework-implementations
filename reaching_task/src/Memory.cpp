#include "reaching_task/Reach_Data.h"
#include "reaching_task/reachTimeService.h"
#include "reaching_task/smaService.h"

#include "Memory.h"

/* Memory class
 * Stores data for use by other nodes and for analysis
 * Sub: Event Handler
 * Pub: 
 * Cli: 
 * Srv: Event Handler, Action Center
 */

Memory::Memory()
{
    _sub = _n.subscribe<reaching_task::Reach_Data>("reach_data", 1, &Memory::callback, this);
    _reachTimeSrv = _n.advertiseService("reach_time", &Memory::serviceReachTime, this);
    _smaSrv = _n.advertiseService("sma", &Memory::serviceSMA, this);
}

bool Memory::serviceReachTime(reaching_task::reachTimeService::Request &req, reaching_task::reachTimeService::Response &res)
{
    if(req.taskNumber > _reachTimes.size() - 1 || _reachTimes.size() == 0)
    {
        res.reachTime = 0;
    }
    else
    {
        res.reachTime = _reachTimes.at(_reachTimes.size() - 1 - req.taskNumber);
    }
    ROS_INFO("request: Final Reaction Time for task attempt: %i", (int)req.taskNumber + 1);
    ROS_INFO("sending back response: [%f]", res.reachTime);
    return true;
}

bool Memory::serviceSMA(reaching_task::smaService::Request &req, reaching_task::smaService::Response &res)
{
    if(req.taskNumber > _smas.size() - 1 || _smas.size() == 0)
    {
        res.sma = 0;
    }
    else
    {
        res.sma = _smas.at(_smas.size() - 1 - req.taskNumber);
    }
    ROS_INFO("request: Final Reaction Time for task attempt: %i", (int)req.taskNumber + 1);
    ROS_INFO("sending back response: [%i]", (int)res.sma);
    return true;
}

void Memory::callback(const reaching_task::Reach_Data msg)
{
    _smas.push_back(msg.sma);
    _reachTimes.push_back(msg.reachTime);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Memory");
    Memory Memory;
    
    ros::spin();
    return 0;
}
