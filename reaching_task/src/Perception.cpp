#include "reaching_task/User_Input.h"
#include "reaching_task/Input.h"
#include "reaching_task/Data.h"

#include "sensor_msgs/Imu.h"
#include "geometry_msgs/Vector3.h"
#include "std_msgs/Int32.h"

#include "Perception.h"

/* Perception class
 * Takes user input and packages relevant information for Event_Handler and Memory
 * Sub: User_Input
 * Pub: Event_Handler, Memory
 * Cli:
 * Srv:
 */

Perception::Perception()
{
    _pubInput = _n.advertise<reaching_task::Input>("input_data", 1);
    _pubData = _n.advertise<reaching_task::Data>("velocity_data", 1);
    _subInput = _n.subscribe<reaching_task::User_Input>("user_input_data", 1, &Perception::callbackInput, this);
    _subData = _n.subscribe<sensor_msgs::Imu>("/imu/data_raw", 1, &Perception::callbackData, this);
}

void Perception::buildOutputMessageInput(const reaching_task::User_Input msg)
{
    _msgInput.Header.stamp = ros::Time::now();
    _msgInput.Header.seq = msg.Header.seq + 1;
    _msgInput.input = msg.input;
}

void Perception::buildOutputMessageData(const sensor_msgs::Imu msg)
{
    _msgInput.Header.stamp = ros::Time::now();
    _msgData.data = msg.angular_velocity;
}

void Perception::callbackInput(const reaching_task::User_Input msg)
{    
    buildOutputMessageInput(msg);
    
    _pubInput.publish(_msgInput);
}

void Perception::callbackData(const sensor_msgs::Imu msg)
{    
    buildOutputMessageData(msg);
    
    _pubData.publish(_msgData);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Perception");
    Perception Perception;
    
    ros::spin();
    return 0;
}
